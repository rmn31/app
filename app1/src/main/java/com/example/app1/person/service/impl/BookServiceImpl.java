package com.example.app1.person.service.impl;

import com.example.app1.person.dto.BookCreateRequest;
import com.example.app1.person.dto.BookUpdateRequest;
import com.example.app1.person.dto.response.ApiResponseCode;
import com.example.app1.person.exception.BusinessException;
import com.example.app1.person.model.Book;
import com.example.app1.person.repository.BookRepository;
import com.example.app1.person.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    @Override
    public List<Book> listAll() {
        return bookRepository.findAll();
    }

    @Override
    public Book create(BookCreateRequest createRequest) {

        var book = new Book()
                .setAuthorName(createRequest.getAuthorName())
                .setBookName(createRequest.getBookName())
                .setReleaseYear(createRequest.getReleaseYear());

        return bookRepository.save(book);
    }

    @Override
    public Book update(BookUpdateRequest updateRequest, Long id) {
        Book book = get(id)
                .setAuthorName(updateRequest.getAuthorName())
                .setBookName(updateRequest.getBookName())
                .setReleaseYear(updateRequest.getReleaseYear());

        return bookRepository.save(book);
    }


    @Override
    public Book get(Long id) {
        Optional<Book> result = bookRepository.findById(id);
        if (result.isEmpty()) {
            throw new BusinessException(ApiResponseCode.NOT_FOUND, Collections.singletonList(id));
        }
        return result.get();
    }

    @Override
    public void delete(Long id) {
        bookRepository.deleteById(id);
    }
}
