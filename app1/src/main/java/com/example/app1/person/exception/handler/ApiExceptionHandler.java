package com.example.app1.person.exception.handler;

import com.example.app1.person.dto.response.ApiResponse;
import com.example.app1.person.dto.response.ApiResponseCode;
import com.example.app1.person.exception.BusinessException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;


@Slf4j
@AllArgsConstructor
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ApiResponse> handleExceptions(Exception e) {
        String message = e.getMessage();
        log.error(message, e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ApiResponse.error(ApiResponseCode.INTERNAL_SERVER_ERROR));
    }

    @ExceptionHandler(value = BusinessException.class)
    public ResponseEntity<ApiResponse> handleBusinessExceptions(BusinessException e) {
        log.error("Business exception occurred on the server", e);
        var cause = e.getCause();
        if (cause != null) {
            logger.error(cause.getMessage(), cause);
        }
        return ResponseEntity.ok(ApiResponse.error(e.getApiResponseCode(), e.getData(), e.getParams()));
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception e, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String message = e.getMessage();
        log.error(message, e);
        ApiResponseCode responseCode = ApiResponseCode.INTERNAL_SERVER_ERROR;
        switch (status) {
            case METHOD_NOT_ALLOWED:
                responseCode = ApiResponseCode.METHOD_NOT_ALLOWED;
                break;
            case UNSUPPORTED_MEDIA_TYPE:
                responseCode = ApiResponseCode.UNSUPPORTED_MEDIA_TYPE;
                break;
            case NOT_ACCEPTABLE:
                responseCode = ApiResponseCode.NOT_ACCEPTABLE;
                break;
            case BAD_REQUEST:
                responseCode = ApiResponseCode.BAD_REQUEST;
                break;
            case NOT_FOUND:
                responseCode = ApiResponseCode.NOT_FOUND;
                break;
            case SERVICE_UNAVAILABLE:
                responseCode = ApiResponseCode.SERVICE_UNAVAILABLE;
                break;
        }
        return ResponseEntity
                .status(status)
                .headers(headers)
                .body(ApiResponse.error(responseCode));
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        return handleValidationExceptions(ex.getBindingResult(), status, ex);
    }

    private ResponseEntity<Object> handleValidationExceptions(BindingResult bindingResult, HttpStatus status, Exception ex) {
        String message = bindingResult
                .getAllErrors()
                .stream()
                .map(objectError -> {
                    if (objectError instanceof FieldError) {
                        return "[" + ((FieldError) objectError).getField() + "] " + objectError.getDefaultMessage();
                    }

                    return objectError.getDefaultMessage();
                })
                .collect(Collectors.joining("\n"));
        return ResponseEntity.status(status)
                .body(ApiResponse.error(ApiResponseCode.BAD_REQUEST, message));
    }
}

