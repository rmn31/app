package com.example.app1.person.controller;

import com.example.app1.person.controller.api.PersonControllerApi;
import com.example.app1.person.dto.PersonCreateRequest;
import com.example.app1.person.dto.PersonUpdateRequest;
import com.example.app1.person.dto.response.ApiResponse;
import com.example.app1.person.model.Person;
import com.example.app1.person.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class PersonController implements PersonControllerApi {

    private final PersonService personService;

    @GetMapping("/all")
    public ResponseEntity<ApiResponse<List<Person>>> getAll() {
        return ResponseEntity.ok(ApiResponse.success(personService.listAll()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<Person>> get(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(ApiResponse.success(personService.get(id)));
    }

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<Person>> add(@RequestBody @Valid PersonCreateRequest createRequest) {
        return ResponseEntity.ok(ApiResponse.success(personService.create(createRequest)));
    }

    @PutMapping("/update")
    public ResponseEntity<ApiResponse<Person>> update(@RequestBody @Valid PersonUpdateRequest updateRequest, Long id) {
        return ResponseEntity.ok(ApiResponse.success(personService.update(updateRequest, id)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        personService.delete(id);
        return ResponseEntity.ok().build();
    }
}
