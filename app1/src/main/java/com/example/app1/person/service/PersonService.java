package com.example.app1.person.service;

import com.example.app1.person.dto.PersonCreateRequest;
import com.example.app1.person.dto.PersonUpdateRequest;
import com.example.app1.person.model.Person;

import java.util.List;

public interface PersonService {
    List<Person> listAll();

    Person create(PersonCreateRequest person);

    Person update(PersonUpdateRequest updateRequest, Long id);

    Person get(Long id);

    void delete(Long id);

}
