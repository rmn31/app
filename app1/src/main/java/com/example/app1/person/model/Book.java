package com.example.app1.person.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Accessors(chain = true)
@Table(name="book")
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long author_id;

    @Column(nullable = false, name = "author_name")
    private String authorName;

    @Column(nullable = false,  name = "book_name")
    private String bookName;

    @Column(nullable = false, name = "release_year")
    private int releaseYear;

}

