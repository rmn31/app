package com.example.app1.person.service.impl;

import com.example.app1.person.dto.PersonCreateRequest;
import com.example.app1.person.dto.PersonUpdateRequest;
import com.example.app1.person.dto.response.ApiResponseCode;
import com.example.app1.person.exception.BusinessException;
import com.example.app1.person.model.Person;
import com.example.app1.person.repository.PersonRepository;
import com.example.app1.person.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Override
    public List<Person> listAll() {
        return personRepository.findAll();
    }

    @Override
    public Person create(PersonCreateRequest createRequest) {

        var person = new Person()
                .setEmail(createRequest.getEmail())
                .setPassword(createRequest.getPassword())
                .setFirstName(createRequest.getFirstName())
                .setLastName(createRequest.getLastName())
                .setMiddleName(createRequest.getMiddleName())
                .setGender(createRequest.getGender());

        return personRepository.save(person);
    }

    @Override
    public Person update(PersonUpdateRequest updateRequest, Long id) {
        Person person = get(id)
                .setEmail(updateRequest.getEmail())
                .setPassword(updateRequest.getPassword())
                .setFirstName((updateRequest.getFirstName()))
                .setLastName(updateRequest.getLastName())
                .setMiddleName(updateRequest.getMiddleName())
                .setGender(updateRequest.getGender());
        return personRepository.save(person);

    }

    @Override
    public Person get(Long id) {
        Optional<Person> result = personRepository.findById(id);
        if (result.isEmpty()) {
            throw new BusinessException(ApiResponseCode.PERSON_NOT_FOUND, Collections.singletonList(id));
        }
        return result.get();
    }

    @Override
    public void delete(Long id) {
        personRepository.deleteById(id);
    }
}


