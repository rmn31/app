package com.example.app1.person.dto.response;

import lombok.Getter;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Getter
public class ApiResponse<T> implements Serializable {

    public static final ApiResponse EMPTY = new ApiResponse<>(EmptyData.INSTANCE, null);
    public static final ApiResponse EMPTY_ARRAY = new ApiResponse<>(Collections.emptyList(), null);

    private final T data;
    private final Error error;

    public ApiResponse() {
        data = null;
        error = null;
    }

    protected ApiResponse(T data, Error error) {
        this.data = data;
        this.error = error;
    }

    public static ApiResponse error(ApiResponseCode apiResponseCode, Object data, List<Object> params) {
        return new ApiResponse<>(data, new Error(apiResponseCode.getCode(), apiResponseCode.getDescription(params)));
    }

    public static ApiResponse error(ApiResponseCode apiResponseCode, List<Object> params) {
        return new ApiResponse<>(null, new Error(apiResponseCode.getCode(), apiResponseCode.getDescription(params)));
    }

    public static ApiResponse<SimpleMessageResponse> message(ApiResponseCode apiResponseCode, List<Object> params) {
        return new ApiResponse<>(new SimpleMessageResponse(apiResponseCode.getCode(), apiResponseCode.getDescription(params)), null);
    }

    public static ApiResponse error(ApiResponseCode apiResponseCode, Object data) {
        return new ApiResponse<>(data, new Error(apiResponseCode.getCode(), apiResponseCode.getDescription()));
    }

    public static ApiResponse error(ApiResponseCode apiResponseCode) {
        return new ApiResponse<>(null, new Error(apiResponseCode.getCode(), apiResponseCode.getDescription()));
    }

    public static ApiResponse<SimpleMessageResponse> message(ApiResponseCode apiResponseCode) {
        return new ApiResponse<>(new SimpleMessageResponse(apiResponseCode.getCode(), apiResponseCode.getDescription()), null);
    }

    public static <T> ApiResponse<T> success(T data) {
        return new ApiResponse<>(data, null);
    }

    @SuppressWarnings("unchecked")
    public static <T> ApiResponse<T> empty() {
        return (ApiResponse<T>) EMPTY;
    }

    @SuppressWarnings("unchecked")
    public static <T> ApiResponse<List<T>> emptyArray() {
        return EMPTY_ARRAY;
    }

    public boolean isError() {
        return error != null;
    }

    @Getter
    public static class Error {

        private final int code;
        private final String description;

        public Error() {
            code = 0;
            description = null;
        }

        private Error(int code, String description) {
            this.code = code;
            this.description = description;
        }
    }

    private static class EmptyData {
        private static final EmptyData INSTANCE = new EmptyData();
    }
}

