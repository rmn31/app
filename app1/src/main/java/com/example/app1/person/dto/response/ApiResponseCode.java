package com.example.app1.person.dto.response;

import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum ApiResponseCode implements Serializable {

    SUCCESS(-1, "Успешно выполнено"),
    INTERNAL_SERVER_ERROR(0, "Внутренняя ошибка сервиса"),
    BAD_REQUEST(1, "Невалидный HTTP запрос"),
    NOT_FOUND(2, "Запрашиваемый ресурс не найден"),
    SERVICE_UNAVAILABLE(3, "Сервис недоступен"),
    UNSUPPORTED_MEDIA_TYPE(4, "Неподдерживаемый тип контента"),
    METHOD_NOT_ALLOWED(5, "Неверный HTTP метод"),
    NOT_ACCEPTABLE(6, "Неприемлемый тип ответа"),
    PERSON_NOT_FOUND(7, "Персона с идентификатором %s не найдена"),
    ;


    private final int code;
    private final String description;

    ApiResponseCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return getDescription(Collections.emptyList());
    }

    public String getDescription(List<Object> params) {
        if (CollectionUtils.isEmpty(params)) {
            return description;
        } else {
            return String.format(description, params.toArray(new Object[0]));
        }
    }

    public static ApiResponseCode findByCode(int code) {
        return Arrays.stream(values()).filter(apiResponseCode -> apiResponseCode.code == code).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Api response code not found with code: " + code));
    }

    public boolean isSuccess() {
        return SUCCESS == this;
    }
}
