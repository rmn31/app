package com.example.app1.person.controller.api;

import com.example.app1.person.dto.PersonCreateRequest;
import com.example.app1.person.dto.PersonUpdateRequest;
import com.example.app1.person.dto.response.ApiResponse;
import com.example.app1.person.exception.BusinessException;
import com.example.app1.person.model.Person;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Tag(name = "PersonControllerApi", description = "API для работы с персонами")
public interface PersonControllerApi {

    @Operation(description = "Получения всех персон")
    ResponseEntity<ApiResponse<List<Person>>> getAll();

    @Operation(description = "Получения персоны по идентификатору")
    ResponseEntity<ApiResponse<Person>> get(@Parameter(description = "идентификатор персоны") Long id) throws BusinessException;

    @Operation(description = "Добавление новой персоны")
    ResponseEntity<ApiResponse<Person>> add(PersonCreateRequest createRequest) throws BusinessException;

    @Operation(description = "Изменение данных персоны")
    ResponseEntity<ApiResponse<Person>> update(PersonUpdateRequest updateRequest, @Parameter(description = "идентификатор персоны") Long id) throws BusinessException;

    @Operation(description = "Удаление персоны по идентификатору")
    ResponseEntity<Void> delete(@PathVariable("id") Long id);
}


