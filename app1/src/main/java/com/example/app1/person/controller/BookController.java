package com.example.app1.person.controller;

import com.example.app1.person.controller.api.BookControllerApi;
import com.example.app1.person.dto.BookCreateRequest;
import com.example.app1.person.dto.BookUpdateRequest;
import com.example.app1.person.dto.response.ApiResponse;
import com.example.app1.person.exception.BusinessException;
import com.example.app1.person.model.Book;
import com.example.app1.person.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RestController
@RequestMapping("/book")
@AllArgsConstructor
public class BookController implements BookControllerApi {

    private final BookService bookService;

    @GetMapping("/all")
    public ResponseEntity<ApiResponse<List<Book>>> getAllBooks() {
        return ResponseEntity.ok(ApiResponse.success(bookService.listAll()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse<Book>> getBook(@PathVariable(name = "id") Long id) throws BusinessException {
        return ResponseEntity.ok(ApiResponse.success(bookService.get(id)));
    }

    @PostMapping("/add")
    public ResponseEntity<ApiResponse<Book>> addBook(@RequestBody @Valid BookCreateRequest createRequest) throws BusinessException {
        return ResponseEntity.ok(ApiResponse.success(bookService.create(createRequest)));
    }

    @PutMapping("/update")
    public ResponseEntity<ApiResponse<Book>> updateBook(@RequestBody @Valid BookUpdateRequest updateRequest, Long id) throws BusinessException {
        return ResponseEntity.ok(ApiResponse.success(bookService.update(updateRequest, id)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBook(@PathVariable("id") Long id) {
        bookService.delete(id);
        return ResponseEntity.ok().build();
    }
}
