package com.example.app1.person.service;

import com.example.app1.person.dto.BookCreateRequest;
import com.example.app1.person.dto.BookUpdateRequest;
import com.example.app1.person.model.Book;

import java.util.List;


public interface BookService {

    List<Book> listAll();

    Book create(BookCreateRequest book);

    Book update(BookUpdateRequest updateRequest, Long id);

    Book get(Long id);

    void delete(Long id);

}
