package com.example.app1.person.controller.api;

import com.example.app1.person.dto.BookCreateRequest;
import com.example.app1.person.dto.BookUpdateRequest;
import com.example.app1.person.dto.response.ApiResponse;
import com.example.app1.person.exception.BusinessException;
import com.example.app1.person.model.Book;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Tag(name = "BookControllerApi", description = "API для работы с книгами")
public interface BookControllerApi {

    @Operation(description = "Получения всех книг")
    ResponseEntity<ApiResponse<List<Book>>> getAllBooks();


    @Operation(description = "Получения книги по идентификатору")
    ResponseEntity<ApiResponse<Book>> getBook(@Parameter(description = "идентификатор книги") Long id) throws BusinessException;

    @Operation(description = "Добавление новой книги")
    ResponseEntity<ApiResponse<Book>> addBook(BookCreateRequest createRequest) throws BusinessException;


    @Operation(description = "Изменение данных книги")
    ResponseEntity<ApiResponse<Book>> updateBook(BookUpdateRequest updateRequest, @Parameter(description = "идентификатор персоны") Long id) throws BusinessException;

    @Operation(description = "Удаление книги по идентификатору")
    ResponseEntity<Void> deleteBook(@PathVariable("id") Long id);
}

