package com.example.app1.person.exception;

import com.example.app1.person.dto.response.ApiResponseCode;
import lombok.Getter;

import java.util.Collections;
import java.util.List;

@Getter
public class BusinessException extends RuntimeException {

    private static final String DELIMITER = ",";

    private final ApiResponseCode apiResponseCode;
    private final List<Object> params;
    private final Object data;

    public BusinessException(ApiResponseCode apiResponseCode, List<Object> params) {
        super(apiResponseCode.getDescription(params));
        this.apiResponseCode = apiResponseCode;
        this.data = null;
        this.params = params;
    }

    public BusinessException(ApiResponseCode apiResponseCode, Object data, List<Object> params) {
        super(apiResponseCode.getDescription(params));
        this.apiResponseCode = apiResponseCode;
        this.params = params;
        this.data = data;
    }

    public BusinessException(ApiResponseCode apiResponseCode, String description, List<Object> params) {
        super(String.join(DELIMITER, apiResponseCode.getDescription(params), description));
        this.apiResponseCode = apiResponseCode;
        this.params = params;
        this.data = null;
    }

    public BusinessException(Throwable cause, ApiResponseCode apiResponseCode, List<Object> params) {
        super(apiResponseCode.getDescription(params), cause);
        this.apiResponseCode = apiResponseCode;
        this.params = params;
        this.data = null;
    }


    public BusinessException(ApiResponseCode apiResponseCode) {
        super(apiResponseCode.getDescription());
        this.apiResponseCode = apiResponseCode;
        this.data = null;
        this.params = Collections.emptyList();
    }

    public BusinessException(ApiResponseCode apiResponseCode, Object data) {
        super(apiResponseCode.getDescription());
        this.apiResponseCode = apiResponseCode;
        this.params = Collections.emptyList();
        this.data = data;
    }

    public BusinessException(ApiResponseCode apiResponseCode, String description) {
        super(String.join(DELIMITER, apiResponseCode.getDescription(), description));
        this.apiResponseCode = apiResponseCode;
        this.params = Collections.emptyList();
        this.data = null;
    }

    public BusinessException(Throwable cause, ApiResponseCode apiResponseCode) {
        super(apiResponseCode.getDescription(), cause);
        this.apiResponseCode = apiResponseCode;
        this.params = Collections.emptyList();
        this.data = null;
    }

    public BusinessException(ApiResponseCode apiResponseCode, Object data, String message) {
        super(message);
        this.apiResponseCode = apiResponseCode;
        this.params = Collections.emptyList();
        this.data = data;
    }

}

