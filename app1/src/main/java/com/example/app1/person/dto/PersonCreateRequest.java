package com.example.app1.person.dto;

import com.example.app1.person.model.Gender;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Accessors(chain = true)
public class PersonCreateRequest {

    @NotNull(message = "Поле email должно быть заполнено")
    @NotBlank(message = "Поле email должно быть заполнено")
    @Pattern(message = "невалидный формат email ", regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
    @Schema(description = "EMAIL персоны", required = true)
    private String email;

    @NotNull(message = "Поле пароль должно быть заполнено")
    @NotBlank(message = "Поле пароль должно быть заполнено")
    @Schema(description = "Пароль персоны", required = true)
    private String password;

    @NotNull(message = "Поле имя должно быть заполнено")
    @NotBlank(message = "Поле имя должно быть заполнено")
    @Schema(description = "Имя", required = true)
    private String firstName;

    @NotNull(message = "Поле фамилия должно быть заполнено")
    @NotBlank(message = "Поле фамилия должно быть заполнено")
    @Schema(description = "Фамилия", required = true)
    private String lastName;

    @Schema(description = "Отчество")
    private String middleName;

    @NotNull(message = "Поле пол должно быть заполнено")
    @Schema(description = "пол")
    private Gender gender;


}
