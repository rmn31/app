package com.example.app1.person.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class BookCreateRequest {

    @NotNull(message = "Поле автор должно быть заполнено")
    @NotBlank(message = "Поле автор должно быть заполнено")
    @Schema(description = "Автор", required = true)
    private String authorName;

    @NotNull(message = "Поле название книги должно быть заполнено")
    @NotBlank(message = "Поле название книги должно быть заполнено")
    @Schema(description = "Название книги", required = true)
    private String bookName;

    @NotNull(message = "Поле год выпуска должно быть заполнено")
    @Schema(description = "Год выпуска книги", required = true)
    private int releaseYear;

}

