create table person
(
    id          bigserial primary key,
    email       text not null,
    password    text not null,
    first_name  text not null,
    last_name   text not null,
    middle_name text
);

create unique index person_email_uindex
    on person (email);
