create table book
(
    author_id    bigserial primary key,
    author_name  text not null,
    book_name   text not null,
    release_year int
);

create unique index book_authorId_uindex
    on book (author_id);
